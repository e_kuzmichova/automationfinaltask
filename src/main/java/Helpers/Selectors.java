package helpers;

import org.openqa.selenium.By;

public interface Selectors {

    //Header
    By CONTACT_US_LINK = By.cssSelector("#contact-link>a");
    By SIGN_IN_LINK = By.className("login");
    By SEARCH_INPUT = By.id("search_query_top");
    By SEARCH_BUTTON = By.className("button-search");
    By CART_LINK = By.cssSelector(".shopping_cart>a");
    By WOMEN_MENU = By.cssSelector(".sf-with-ul[title='Women']");
    By SHIRTS_MENU = By.xpath("//a[@title='Tops']/..//a[@title='T-shirts']");

    //Item
    By CONTINUE_SHOPPING = By.cssSelector(".continue.btn.btn-default.button.exclusive-medium");
    By PROCEED_TO_CHECKOUT_BUTTON = By.cssSelector(".btn-default.button-medium");

    //AuthenticationPage
    By CREATE_ACCOUNT_EMAIL_INPUT = By.id("email_create");
    By CREATE_ACCOUNT_BUTTON = By.id("SubmitCreate");

    //CartPage
    By ALERT_WARNING = By.cssSelector(".alert.alert-warning");
    By CART_PROCEED_TO_CHECKOUT_BUTTON = By.className("standard-checkout");

    //ContactUsPage
    By SUBJECT_HEADING_SELECT = By.cssSelector("#id_contact.form-control");
    By EMAIL_INPUT = By.id("email");
    By ORDER_REFERENCE_INPUT = By.id("id_order");
    By ATTACH_FILE_INPUT = By.id("fileUpload");
    By MESSAGE_INPUT = By.id("message");
    By SEND_BUTTON = By.id("submitMessage");
    By ALERT_SUCCESS = By.cssSelector(".alert.alert-success");
    By ALERT_EMPTY_MESSAGE = By.xpath("//div/ol/li[text()='The message cannot be blank.']");

    //CreateAccountPage
    By FIRST_NAME_INPUT = By.id("customer_firstname");
    By LAST_NAME_INPUT = By.id("customer_lastname");
    By PASSWORD_INPUT = By.id("passwd");
    By ADDRESS_INPUT_1 = By.id("address1");
    By CITY_INPUT = By.id("city");
    By STATE_SELECT = By.id("id_state");
    By POSTCODE_INPUT = By.id("postcode");
    By COUNTRY_SELECT = By.id("id_country");
    By MOBILE_PHONE_INPUT = By.id("phone_mobile");
    By REGISTER_BUTTON = By.id("submitAccount");

    //MyAccountPage
    By MY_ACCOUNT_HEADING = By.xpath("//h1[@class='page-heading'][text()='My account']");

    //OrderHistoryPage
    By DETAILS_BUTTON = By.cssSelector(".history_detail.footable-last-column .btn.btn-default.button.button-small");
    By ITEM_LABEL = By.xpath("//label[contains(text(), 'Blouse')]");

    //ProceedToCheckoutPage
    By PROCEED_BUTTON = By.cssSelector(".button.btn.btn-default.button-medium[type='submit']");
    By TERMS_OF_SERVICE_CHECKBOX = By.id("cgv");
    By PAY_BY_BANK_WIRE_LINK = By.className("bankwire");
    By BACK_TO_ORDERS_LINK = By.cssSelector(".button-exclusive.btn.btn-default");
}
