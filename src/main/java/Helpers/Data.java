package helpers;

import java.io.File;

public interface Data {

    //tests
    String URL = "http://automationpractice.com/index.php";
    String SEARCH_ITEM = "Blouse";
    String TSHIRT = "T-shirts";

    String EMAIL = "@gmail.com";

    //ContactUsPage
    String CUSTOMER_SERVICE = "2";
    String ORDER_REFERENCE = "0123456789";
    String FILE_PATH = new File("src/resources/testFile.txt").getAbsolutePath();
    String MESSAGE = "Test message";

    //CreateAccountPage
    String FIRST_NAME = "User";
    String LAST_NAME = "User";
    String PASSWORD = "secret_password";
    String ADDRESS = "Pop str, 33 - 333";
    String CITY = "San Francisco";
    String STATE_CALIFORNIA = "5";
    String POSTCODE = "12345";
    String COUNTRY_US = "21";
    String PHONE_NUMBER = "80809999999";
}
