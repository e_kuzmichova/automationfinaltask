package pages;

import org.openqa.selenium.WebDriver;
import pages.parts.Item;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    private Item item = new Item(getDriver());

    public Item getItem() {
        return item;
    }
}
