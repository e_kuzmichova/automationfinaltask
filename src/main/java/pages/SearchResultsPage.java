package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.parts.Item;

public class SearchResultsPage extends BasePage {

    private Item item = new Item(getDriver());

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public Item getItem() {
        return item;
    }

    public boolean abilityToSearchItems(String itemName) {
        By itemSelector = item.searchedItem(itemName);
        return isElementDisplayed(itemSelector);
    }
}
