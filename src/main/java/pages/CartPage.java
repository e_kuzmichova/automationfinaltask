package pages;

import helpers.Selectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage extends BasePage {

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public boolean isItemInCart(String name) {
        return isElementDisplayed(By.cssSelector(String.format(".product-name>a[title*='%s']", name)));
    }

    public boolean isCartEmpty() {
        return isElementDisplayed(Selectors.ALERT_WARNING);
    }

    public void deleteItemFromCart(String name) {
        getDriver().findElement(By.xpath(String.format("//a[contains(text(), %s)]/../../../td[@data-title='Delete']//a", name))).click();
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOfElementLocated(Selectors.ALERT_WARNING));
    }

    public AuthenticationPage proceedToCheckout() {
        getDriver().findElement(Selectors.CART_PROCEED_TO_CHECKOUT_BUTTON).click();
        return new AuthenticationPage(getDriver());
    }
}
