package pages;

import helpers.Selectors;
import org.openqa.selenium.WebDriver;

public class OrderHistoryPage extends BasePage {

    public OrderHistoryPage(WebDriver driver) {
        super(driver);
    }

    public void viewDetails() {
        getDriver().findElement(Selectors.DETAILS_BUTTON).click();
    }

    public boolean isItemInOrderHistory() {
        return isElementDisplayed(Selectors.ITEM_LABEL);
    }
}
