package pages;

import helpers.Data;
import helpers.Selectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateAccountPage extends BasePage {

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    private void createNewAccount() {
        getDriver().findElement(Selectors.FIRST_NAME_INPUT).sendKeys(Data.FIRST_NAME + getRandomChars());
        getDriver().findElement(Selectors.LAST_NAME_INPUT).sendKeys(Data.LAST_NAME);
        getDriver().findElement(Selectors.PASSWORD_INPUT).sendKeys(Data.PASSWORD);
        getDriver().findElement(Selectors.ADDRESS_INPUT_1).sendKeys(Data.ADDRESS);
        getDriver().findElement(Selectors.CITY_INPUT).sendKeys(Data.CITY);
        new Select(getDriver().findElement(Selectors.STATE_SELECT)).selectByValue(Data.STATE_CALIFORNIA);
        getDriver().findElement(Selectors.POSTCODE_INPUT).sendKeys(Data.POSTCODE);
        new Select(getDriver().findElement(Selectors.COUNTRY_SELECT)).selectByValue(Data.COUNTRY_US);
        getDriver().findElement(Selectors.MOBILE_PHONE_INPUT).sendKeys(Data.PHONE_NUMBER);
        getDriver().findElement(Selectors.REGISTER_BUTTON).click();
    }

    public MyAccountPage goToMyAccount() {
        createNewAccount();
        return new MyAccountPage(getDriver());
    }

    public ProceedToCheckoutPage proceedToCheckout() {
        createNewAccount();
        return new ProceedToCheckoutPage(getDriver());
    }
}
