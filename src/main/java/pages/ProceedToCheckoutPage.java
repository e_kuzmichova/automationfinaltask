package pages;

import helpers.Selectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProceedToCheckoutPage extends BasePage {

    public ProceedToCheckoutPage(WebDriver driver) {
        super(driver);
    }

    public void addressStep() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOfElementLocated(Selectors.PROCEED_BUTTON));
        getDriver().findElement(Selectors.PROCEED_BUTTON).click();
    }

    public void shippingStep() {
        getDriver().findElement(Selectors.TERMS_OF_SERVICE_CHECKBOX).click();
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOfElementLocated(Selectors.PROCEED_BUTTON));
        getDriver().findElement(Selectors.PROCEED_BUTTON).click();
    }

    public void paymentStep() {
        getDriver().findElement(Selectors.PAY_BY_BANK_WIRE_LINK).click();
        getDriver().findElement(Selectors.PROCEED_BUTTON).click();
    }

    public OrderHistoryPage backToOrders() {
        getDriver().findElement(Selectors.BACK_TO_ORDERS_LINK).click();
        return new OrderHistoryPage(getDriver());
    }
}
