package pages.parts;

import helpers.Selectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pages.CartPage;

public class Item {

    private WebDriver driver;

    public Item(WebDriver driver) {
        this.driver = driver;
    }

    public By searchedItem(String itemName) {
        return By.xpath(String.format("//div[@id='center_column']//a[contains(text(),'%s')]", itemName));
    }

    private void addButtonVisible(String itemName) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(searchedItem(itemName))).build().perform();
    }

    public void addToCart(String itemName) {
        addButtonVisible(itemName);
        driver.findElement(By.xpath(String.format("//a[@title='%s']/../../../..//a[@title='Add to cart']", itemName))).click();
    }

    public void continueShopping() {
        driver.findElement(Selectors.CONTINUE_SHOPPING).click();
    }

    public CartPage proceedToCheckout() {
        driver.findElement(Selectors.PROCEED_TO_CHECKOUT_BUTTON).click();
        return new CartPage(driver);
    }
}
