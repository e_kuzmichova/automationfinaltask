package pages.parts;

import helpers.Selectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pages.AuthenticationPage;
import pages.CartPage;
import pages.ContactUsPage;
import pages.SearchResultsPage;

public class Header {

    private WebDriver driver;

    public Header(WebDriver driver) {
        this.driver = driver;
    }

    public ContactUsPage contactUsLink() {
        driver.findElement(Selectors.CONTACT_US_LINK).click();
        return new ContactUsPage(driver);
    }

    public AuthenticationPage signInLink() {
        driver.findElement(Selectors.SIGN_IN_LINK).click();
        return new AuthenticationPage(driver);
    }

    public SearchResultsPage searchItem(String item) {
        driver.findElement(Selectors.SEARCH_INPUT).sendKeys(item);
        driver.findElement(Selectors.SEARCH_BUTTON).click();
        return new SearchResultsPage(driver);
    }

    public CartPage goToCart() {
        driver.findElement(Selectors.CART_LINK).click();
        return new CartPage(driver);
    }

    public SearchResultsPage goToTShirtsPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Selectors.WOMEN_MENU)).build().perform();
        driver.findElement(Selectors.SHIRTS_MENU).click();
        return new SearchResultsPage(driver);
    }
}
