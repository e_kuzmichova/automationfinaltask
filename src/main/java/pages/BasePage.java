package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import pages.parts.Header;

public abstract class BasePage {

    private WebDriver driver;
    private Header header;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.header = new Header(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public Header getHeader() {
        return header;
    }

    public boolean isElementDisplayed(By element) {
        try {
            return driver.findElement(element).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public String getRandomChars() {
        StringBuffer result = new StringBuffer("");
        String chars = "abcdefghijklmnopqrstuvwxyz";
        while(result.length() < 6) {
            result.append(chars.charAt((int) (Math.random() * chars.length())));
        }
        return result.toString();
    }
}
