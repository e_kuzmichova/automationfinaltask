package pages;

import helpers.Data;
import helpers.Selectors;
import org.openqa.selenium.WebDriver;

public class AuthenticationPage extends BasePage {

    public AuthenticationPage(WebDriver driver) {
        super(driver);
    }

    private void setEmail() {
        getDriver().findElement(Selectors.CREATE_ACCOUNT_EMAIL_INPUT).sendKeys(getRandomChars() + Data.EMAIL);
    }

    private void createButton() {
        getDriver().findElement(Selectors.CREATE_ACCOUNT_BUTTON).click();
    }

    public CreateAccountPage createAccount() {
        setEmail();
        createButton();
        return new CreateAccountPage(getDriver());
    }
}
