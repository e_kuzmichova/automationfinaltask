package pages;

import helpers.Selectors;
import org.openqa.selenium.WebDriver;

public class MyAccountPage extends BasePage {

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public boolean myAccountHeadingIsVisible() {
        return isElementDisplayed(Selectors.MY_ACCOUNT_HEADING);
    }
}
