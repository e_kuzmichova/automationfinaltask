package pages;

import helpers.Data;
import helpers.Selectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class ContactUsPage extends BasePage {

    public ContactUsPage(WebDriver driver) {
        super(driver);
    }

    public void selectSubjectHeading() {
        new Select(getDriver().findElement(Selectors.SUBJECT_HEADING_SELECT)).selectByValue(Data.CUSTOMER_SERVICE);
    }

    public void setEmail() {
        getDriver().findElement(Selectors.EMAIL_INPUT).sendKeys(getRandomChars() + Data.EMAIL);
    }

    public void setOrderReference() {
        getDriver().findElement(Selectors.ORDER_REFERENCE_INPUT).sendKeys(Data.ORDER_REFERENCE);
    }

    public void attachFile() {
        getDriver().findElement(Selectors.ATTACH_FILE_INPUT).sendKeys(Data.FILE_PATH);
    }

    public void setMessage() {
        getDriver().findElement(Selectors.MESSAGE_INPUT).sendKeys(Data.MESSAGE);
    }

    public void sendButton() {
        getDriver().findElement(Selectors.SEND_BUTTON).click();
    }

    public boolean alertSuccessMessageIsVisible() {
        return isElementDisplayed(Selectors.ALERT_SUCCESS);
    }

    public boolean alertEmptyMessageIsVisible() {
        return isElementDisplayed(Selectors.ALERT_EMPTY_MESSAGE);
    }
}
