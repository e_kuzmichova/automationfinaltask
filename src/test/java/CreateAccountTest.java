import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AuthenticationPage;
import pages.CreateAccountPage;
import pages.MyAccountPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class CreateAccountTest extends BaseTest {

    private AuthenticationPage authenticationPage;
    private CreateAccountPage createAccountPage;
    private MyAccountPage myAccountPage;

    @BeforeMethod
    public void setup() {
        super.setup();
        authenticationPage = getHomePage().getHeader().signInLink();
        createAccountPage = authenticationPage.createAccount();
        myAccountPage = createAccountPage.goToMyAccount();
    }

    @AfterMethod
    public void teardown() {
        super.teardown();
    }

    @Test
    @TestCaseId("E-3")
    @Description("Test verifies the ability to register")
    public void successfulRegistrationTest() {
        Assert.assertTrue(myAccountPage.myAccountHeadingIsVisible(), "Registration error!");
    }
}
