import helpers.Data;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.SearchResultsPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class SearchItemTest extends BaseTest {

    private SearchResultsPage searchResultsPage;

    @BeforeMethod
    public void setup() {
        super.setup();
    }

    @AfterMethod
    public void teardown() {
        super.teardown();
    }

    @Test
    @TestCaseId("E-4")
    @Description("Test verifies the ability to search items")
    public void abilityToSearchItemsTest() {
        searchResultsPage = getHomePage().getHeader().searchItem(Data.SEARCH_ITEM);
        Assert.assertTrue(searchResultsPage.abilityToSearchItems(Data.SEARCH_ITEM), "Item(s) not found!");
    }

    @Test
    @TestCaseId("E-6")
    @Description("Test verifies the ability to view catalog")
    public void catalogTest() {
        searchResultsPage = getHomePage().getHeader().goToTShirtsPage();
        Assert.assertTrue(searchResultsPage.abilityToSearchItems(Data.TSHIRT), "Item(s) not found!");
    }
}
