import helpers.Data;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.SearchResultsPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class CartTest extends BaseTest {

    private SearchResultsPage searchResultsPage;
    private CartPage cartPage;

    @BeforeMethod
    public void setup() {
        super.setup();
        searchResultsPage = getHomePage().getHeader().searchItem(Data.SEARCH_ITEM);
        searchResultsPage.getItem().addToCart(Data.SEARCH_ITEM);
        searchResultsPage.getItem().continueShopping();
        cartPage = searchResultsPage.getHeader().goToCart();
    }

    @AfterMethod
    public void teardown() {
        super.teardown();
    }

    @Test
    @TestCaseId("E-5")
    @Description("Test verifies the ability to add an item to cart")
    public void itemAddedToCartTest() {
        Assert.assertTrue(cartPage.isItemInCart(Data.SEARCH_ITEM), "Item not found!");
    }

    @Test
    @TestCaseId("E-5")
    @Description("Test verifies the ability to delete an item from cart")
    public void itemDeletedFromCartTest() {
        cartPage.deleteItemFromCart(Data.SEARCH_ITEM);
        Assert.assertTrue(cartPage.isCartEmpty(), "Item not delete!");
    }
}
