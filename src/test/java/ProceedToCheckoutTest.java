import helpers.Data;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class ProceedToCheckoutTest extends BaseTest {

    private CartPage cartPage;
    private AuthenticationPage authenticationPage;
    private CreateAccountPage createAccountPage;
    private ProceedToCheckoutPage proceedToCheckoutPage;
    private OrderHistoryPage orderHistoryPage;

    @BeforeMethod
    public void setup() {
        super.setup();
    }

    @AfterMethod
    public void teardown() {
        super.teardown();
    }

    @Test
    @TestCaseId("E-7")
    @Description("Test verifies order history after checkout")
    public void orderHistoryTest() {
        getHomePage().getItem().addToCart(Data.SEARCH_ITEM);
        cartPage = getHomePage().getItem().proceedToCheckout();
        authenticationPage = cartPage.proceedToCheckout();
        createAccountPage = authenticationPage.createAccount();
        proceedToCheckoutPage = createAccountPage.proceedToCheckout();
        proceedToCheckoutPage.addressStep();
        proceedToCheckoutPage.shippingStep();
        proceedToCheckoutPage.paymentStep();
        orderHistoryPage = proceedToCheckoutPage.backToOrders();
        orderHistoryPage.viewDetails();
        Assert.assertTrue(orderHistoryPage.isItemInOrderHistory(), "Item not found!");
    }
}
