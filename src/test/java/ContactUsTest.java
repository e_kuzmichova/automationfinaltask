import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ContactUsPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class ContactUsTest extends BaseTest {

    private ContactUsPage contactUsPage;

    @BeforeMethod
    public void setup() {
        super.setup();
        contactUsPage = getHomePage().getHeader().contactUsLink();
        contactUsPage.selectSubjectHeading();
        contactUsPage.setEmail();
        contactUsPage.setOrderReference();
        contactUsPage.attachFile();
    }

    @AfterMethod
    public void teardown() {
        super.teardown();
    }

    @Test
    @TestCaseId("E-1")
    @Description("Test verifies that contact us form sends successfully")
    public void contactUsFormSendsSuccessfullyTest() {
        contactUsPage.setMessage();
        contactUsPage.sendButton();
        Assert.assertTrue(contactUsPage.alertSuccessMessageIsVisible(), "Contact us form fails!");
    }

    @Test
    @TestCaseId("E-2")
    @Description("Test verifies that error message appears if Message area is empty")
    public void errorMessage() {
        contactUsPage.sendButton();
        Assert.assertTrue(contactUsPage.alertEmptyMessageIsVisible(), "Error message is not displayed");
    }
}
