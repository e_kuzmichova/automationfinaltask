import helpers.Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class BaseTest{

    private WebDriver driver;
    private HomePage homePage;

    public HomePage getHomePage() {
        return homePage;
    }

    @BeforeMethod
    public void setup() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(Data.URL);
        homePage = new HomePage(driver);
    }

    @AfterMethod
    public void teardown() {
        driver.close();
    }
}
